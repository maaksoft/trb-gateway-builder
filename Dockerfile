FROM rust:latest

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        clang iptables-dev libpcap-dev && \
    rm -rf /var/lib/apt/lists/*
